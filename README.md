# Escape pass

Ett pass kostar x pengar, exempelvis 1000 SEK och är "laddat" med 20 spelkrediter (värda 50 SEK styck). Varje spel kostar så många krediter som varje escaperummet vill att det skall kosta, detta för att komma runt att vi har olika priser och betalningsmodeller. Hos The Alley kanske det kostar 10 krediter per person medan hos Escape Husvagn kanske det kostar 15 krediter för ett spel.

När kund kommer till anläggning för att spela har den med sig kortet, med en QR-kod på. Den telefon som finns i anläggningen skannar QR-koden och får genast upp info om hur många krediter det varit på kortet och hur många som är kvar samt en möjlighet att debitera för så många krediter som kunden vill spela för. Det sättet som en anläggningstelefon "identifierar sig" för systemet är genom att skanna en anläggningsspecifik QR-kod en gång.

Om en annan telefon scannar QR-koden kommer det upp information om hur många krediter som är kvar på kortet och det går inte att debitera.

När en gäst spelar på en anläggning dras det antal krediter av från kortets saldo som kunden spelat för och det sparas centralt. Så det kommer inte att gå att fuska genom att göra kopior på kortet och använda dem samtidigt. Det gör också att det finns ett ställe där vi kan se hur många krediter som spenderats på varje anläggning så att vi kan fördela pengarna varje månad (eller hur ofta vi bestämmer)

Bokföringsmässigt hanterar vi detta som presentkort på The Alley och som försäljning mot faktura på de andra anläggningarna.

```mermaid
sequenceDiagram
	participant The Alley
	participant Escape room 1
	participant Escape room 2
	participant Gäst
	Gäst->>The Alley: Köper kort

	Escape room 1->>Gäst: Spelar
	Escape room 1-->>The Alley: Rapporterar spel
	The Alley->>Escape room 1: Betalar för spelade spel
	Escape room 2->>Gäst: Spelar
	Escape room 2-->>The Alley: Rapporterar spel
	The Alley->>Escape room 2: Betalar för spelade spel
	The Alley->>Gäst: Spelar
	loop
		The Alley-->>The Alley: Rapporterar spel
	end

	loop
		The Alley->>The Alley: Betalar för spelade spel
  end
Note right of Gäst: När Gäst spelar<br>registeras det av<br>gästvärd
Note left of The Alley: Prickad linje betyder information<br>Heldragen betyder<br>värde (pengar eller spel)
```
